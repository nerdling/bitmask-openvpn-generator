SHELL := bash

VE ?= .venv
VE_BIN ?= $(VE)/bin
PYTHON_FOR_VENV := python3
PYTHON := $(VE_BIN)/python3

#### Typical targets ####

##> : default target is help
.PHONY: default
default: help

.PHONY: help # this help text
help:
	@# use any python
	@python makefile_help.py $(MAKEFILE_LIST)

.PHONY: all # install deps and run the main script
all:
	$(PYTHON_FOR_VENV) -m venv .venv
	$(VE_BIN)/pip install -r requirements.txt
	$(PYTHON) script.py

$(VE):
	$(PYTHON_FOR_VENV) -m venv "$(VE)"

$(VE_BIN)/pip-sync: $(VE)
	$(PYTHON) -m pip install pip-tools wheel

.PHONY: clean # removes artifacts
clean:
	$(RM) -r $(VE)

.PHONY: build # install python dependencies into a venv
build: install-requirements

.PHONY: install-requirements
install-requirements: $(VE_BIN)/pip-sync requirements.txt
	$(VE_BIN)/pip-sync requirements.txt

requirements.txt:
	$(SHELL) requirements-update.sh

#### Maintenance targets ####
.PHONY: dist-clean # complete cleanup
dist-clean: clean
	git clean -dfx

.PHONY: update # upgrade requirements file
update: | clean requirements.txt install-requirements
