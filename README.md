# Bitmask OpenVPN Configuration Generator

[Riseup](https://riseup.net) and [Calyx Institute](https://www.calyxinstitute.org/) both rely on branded [Bitmask](https://bitmask.net/) VPN clients (developed by [LEAP](https://leap.se/)) (Riseup's can be found [here](https://riseup.net/vpn#download-riseupvpn) and Calyx's [here](https://f-droid.org/en/packages/org.calyxinstitute.vpn/)). You can also use the general (unbranded) [Bitmask clients](https://bitmask.net/en/install) to connect to both Riseup and Calyx VPN servers.

However there are some Bitmask client compatibility issues, limitations, and no iOS client [yet](https://nitter.snopyta.org/leapcode/status/1243607377569513472). Also server location is determined based on the geolocation of the connecting user's IP address which may not always be desired and isn't customizable through the UI. Some hidden options are supported via command-line argument (`/snap/bin/riseup-vpn.launcher -help`) but again these aren't exposed through the UI.

After analyzing the [Bitmask client source code](https://0xacab.org/leap/bitmask-vpn) and reviewing [LEAP's open API for their Bonafide protocol](https://leap.se/en/docs/design/bonafide) with [Postman](https://www.postman.com/), I was able to write a Python script for generating OpenVPN configuration files (.ovpn) to connect to Riseup and Calyx's VPN servers. This type of file can be used with any software that supports OpenVPN.

Hopefully this complements the available connection options for Riseup and Calyx VPN users. Please donate to Calyx, LEAP, and Riseup if you rely on their services!

## Donate

- [Riseup](https://riseup.net/en/donate)
- [Calyx Institute](https://members.calyxinstitute.org/donate)
- [LEAP](https://leap.se/en/about-us/donate)

## Requirements

You'll need Python 3 installed along with dependencies from the requirements.txt file:
```
$ make build
```

## Usage

```
$ make all
```

![Demo](demo.mp4)

No account required. **Client certificates are apparently valid for 3 months.** After which you'll need to request another client certificate / private key pair from the provider's [/cert endpoint](https://leap.se/en/docs/design/bonafide#get-a-vpn-client-certificate) by running the script again.

Notice: Windows users should add the [`block-outside-dns` option to prevent DNS leaks](https://www.dnsleaktest.com/how-to-fix-a-dns-leak.html).

I recommend [Passepartout](https://passepartoutvpn.app/) as an OpenVPN client for iOS users.

### Run from terminal
```
sudo openvpn --config *.ovpn
```

### Check certificates and private key

For Linux distributions you can use [gcr](https://gitlab.gnome.org/GNOME/gcr).
```
sudo apt install gcr
gcr-viewer *.ovpn
```

![GCR Viewer](gcr.png)

## Supported RiseupVPN Options

The Bonafide API for Riseup does not return all supported protocols and ports. I've listed what appears to be available below as of 12/25/20.

1. Amsterdam, NL (hornero.riseup.net / 212.129.4.141)
2. Amsterdam, NL (mockingjay.riseup.net / 212.129.37.129)
3. Amsterdam, NL (redshank.riseup.net / 212.83.165.160)
4. Amsterdam, NL (shag.riseup.net / 212.83.182.127)
5. Miami, US (crane.riseup.net / 37.218.244.249)
6. Miami, US (limpkin.riseup.net / 37.218.244.251)
7. Montreal, CA (gaei.riseup.net / 199.58.83.12)
8. Montreal, CA (yal.riseup.net / 199.58.83.10)
9. New York City, US (starling.riseup.net / 185.220.103.12)
10. Paris, FR (fournier.riseup.net / 212.129.4.124)
11. Paris, FR (hoatzin.riseup.net / 212.83.143.67)
12. Paris, FR (hirondelle.riseup.net / 212.83.144.12)
13. Paris, FR (pie.riseup.net / 212.83.146.228)
14. Paris, FR (mouette.riseup.net / 163.172.126.44)
15. Paris, FR (zarapito.riseup.net / 212.129.62.247)
16. Seattle, US (garza.riseup.net / 198.252.153.28)

## Supported CalyxVPN Options

The Bonafide API for Calyx does not return all supported protocols and ports. I've listed what appears to be available below.

1. New York City, US (vpn2.calyx.net / 162.247.73.193)

## Development

For API usage reference and example responses you can import the `*.postman_collection.json` file into [Postman](https://www.postman.com/). You'll need to [set Riseup or Calyx's CA certificate in Postman](https://blog.postman.com/2017/12/05/set-and-view-ssl-certificates-with-postman/) for some requests that require it.

You can download Postman for Linux through:
- [Flathub](https://flathub.org/apps/details/com.getpostman.Postman)
- [Snapcraft](https://snapcraft.io/postman)
- [Postman.com](https://www.postman.com/downloads/)

## Resources
- [Bitmask's supported OpenVPN configurations](https://bitmask.net/en/features/cryptography#encrypted-tunnel-openvpn)
- [LEAP project source code](https://0xacab.org/leap)
- [Reference manual for OpenVPN 2.4](https://openvpn.net/community-resources/reference-manual-for-openvpn-2-4/)
- [LEAP technical documentation](https://leap.se/en/docs)
- [Bitmask's VPN client help documentation](https://bitmask.net/en/help/vpn)
- [LEAP platform limitations](https://leap.se/en/docs/tech/limitations)
- [How to parse an X509 certificate in Python](https://web.archive.org/web/20191001225633/http://www.zedwood.com/article/python-openssl-x509-parse-certificate)
- [Current open issues for Bitmask's desktop clients](https://0xacab.org/leap/bitmask-vpn/-/issues)

## Ideas
- Rewrite the mess of code.
- Add requirements.txt for dependency installation.
- Add support for all supported protocols and ports (UDP / 1194) even though they aren't returned from Bonafide API.
- Command line arguments:
	- Option for connecting to API IP rather than URL ([ref](https://0xacab.org/leap/bitmask-vpn/-/merge_requests/89/diffs))
	- Option for retaining client certificate and private key
	- Option for connecting to closest server (using geolocation endpoint (https://api.black.riseup.net:9001/json))
	- Option for connecting to fastest server
	- Option for generating ovpn files for multiple or all gateways at once
